@extends('layout.app_layout')
@section('content')
	<ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route('solicitud.index')}}">Solicitudes</a></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
    </ol>
	<div class="container-fluid mb-4">
        <div class="bg-white py-3 row">
          	<div class="col-sm-6 d-flex align-items-center">
            	<h3 class="mb-0">Solicitudes</h3>
          	</div>
          	<div class="col-sm-6 d-flex align-items-center justify-content-end">
            	<a href="{{ route('solicitud.crear')}}" class="btn btn-success">Agregar solicitud</a>
          	</div>
        </div>
        <form method="GET" action="{{ route('solicitud.index') }}">
	        <div class="form-group">
	          <label for="sel1">Filtrar por Cliente:</label>
	          <select class="form-control" name="cliente_id" id="sel1">
	            <option value=""> Seleccionar cliente</option>
	            @foreach($clientes as $cliente)
	              <option value="{{$cliente->id}}">{{$loop->iteration}}.- {{$cliente->razon_social}}</option>
	            @endforeach
	          </select>
	        </div>
	        <div class="form-group">
	          <button class="btn btn-success">Filtrar</button>
	        </div>
	    </form>
        <div class="animated fadeIn">
          <div class="row bg-white">
            <table class="table">
              	<thead class="thead-dark">
	                <tr>
	                  <th scope="col">#</th>
	                  <th scope="col">Cliente</th>
	                  <th scope="col">Contacto</th>
	                  <th scope="col">Descripción</th>
	                  <th scope="col">Estado</th>
	                  <th scope="col">Fecha</th>
	                  <th scope="col">Acciones</th>
	                </tr>
              	</thead>
              	<tbody>
                	@foreach ($solicitudes as $solicitud)
                      	<tr>
	                        <th scope="row"> {{ $loop->iteration }} </th>
	                        <td>{{ $solicitud->cliente->razon_social }}</td>
	                        <td>{{ $solicitud->contacto->nombre }}</td>
	                        <td>{{ $solicitud->descripcion}}</td>
	                        <td>
	                          <a href="">
	                              <button type="button" class="btn btn-primary">ver</button>
	                          </a>
	                          <a href="">
	                              <button type="button" class="btn btn-info">Editar</button>
	                          </a>
	                          <a href="">
	                              <button type="button" class="btn btn-info">Procesar</button>
	                          </a>
	                          <!-- Button trigger modal -->
	                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarModal{{$solicitud->id}}">
	                            Eliminar
	                          </button>

	                          <!-- Modal -->
	                          	<div class="modal fade" id="eliminarModal{{$solicitud->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	                            	<div class="modal-dialog" role="document">
	                              		<div class="modal-content">
	                                		<div class="modal-header">
	                                  			<h5 class="modal-title" id="exampleModalLabel">Eliminar Solicitud</h5>
	                                  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                                    			<span aria-hidden="true">&times;</span>
	                                  			</button>
	                                		</div>
	                                		<div class="modal-body">
	                                  			¿Está seguro que desea eliminar esta solicitud?
	                                		</div>
	                                		<div class="modal-footer">
	                                  			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	                                  			<a class="btn btn-primary" href="{{ route('solicitud.eliminar', [ 'id'=>$solicitud->id])}}">
	                                    			Eliminar
	                                  			</a>
	                                		</div>
	                              		</div>
	                            	</div>
	                          	</div>
                        	</td>
                      	</tr>
                	@endforeach
              	</tbody>
            </table>
        </div>
    </div>
</div>

@endsection