@extends('layout.app_layout')
@section('content')
		<ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route('solicitud.index')}}">Solicitudes</a></li>
        <li class="breadcrumb-item"><a href="{{ route('solicitud.crear')}}">Crear Solicitud</a></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
    </ol>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card-header">
        <form name="create_servicio" method="post" action="{{ route('solcitud.guardar')}}">
            {{ csrf_field() }}
            <h3 class="card-header">Datos de la Solicitud</h3>
	        <div class="card-block">
				<h4>Nombre</h4>
				<input type="text" name="nombre" class="form-control" id="inputEmail4" placeholder="nombre">
			</div>
			<div class="card-block">
				<h4>Descripción</h4>
				<textarea class="form-control" id="descripcion" name="descripcion" rows="3">Ingrese la descripción</textarea>
			</div>
			<div class="container">
			    <div class="row">
			        <div class='col-sm-6'>
			        	<h4>Fecha de Entrega</h4>
			            <input type='date' name="fecha_entrega" class="form-control" id='datetimepicker4' />
			        </div>
			    </div>
			</div>
			<div class="card-block">
	          <label for="sel1">Seleccionar por Cliente:</label>
	          <select class="form-control" name="id_cliente" id="sel1">
	            <option value=""> Seleccionar cliente</option>
	            @foreach($clientes as $cliente)
	              <option value="{{$cliente->id}}">{{$loop->iteration}}.- {{$cliente->razon_social}}</option>
	            @endforeach
	          </select>
			</div>
			<div class="card-block">
	          <label for="sel1">Seleccionar por Usuario:</label>
	          <select class="form-control" name="id_user" id="sel1">
	            <option value=""> Seleccionar usuario</option>
	            @foreach($users as $usuario)
	              <option value="{{$usuario->id}}">{{$loop->iteration}}.- {{$usuario->nombre}}</option>
	            @endforeach
	          </select>
			</div>
			<div class="card-block">
			    <label for="id_categoria">Seleccionar un categoría:</label>
			    <select class="form-control" name="id_categoria" id="id_categoria">
			    	<option value=""> Seleccionar categoría</option>
			      @foreach($categorias as $categoria)
			      	<option value="{{ $categoria->id }}">{{$loop->iteration}}.- {{ $categoria->nombre }}</option>
			      @endforeach
			    </select>
			</div>
			<div class="card-block">
			    <label for="id_categoria">Seleccionar un tipo:</label>
			    <select class="form-control" name="id_tipo" id="id_tipo">
			    	<option value=""> Seleccionar tipo</option>
			      @foreach($tipos as $tipo)
			      	<option value="{{ $tipo->id }}">{{$loop->iteration}}.- {{ $tipo->tipo }}</option>
			      @endforeach
			    </select>
			</div>
			<div class="card-block">
			    <label for="id_categoria">Seleccionar la fuente:</label>
			    <select class="form-control" name="id_fuente" id="id_fuente">
			    	<option value=""> Seleccionar fuente</option>
			      @foreach($fuentes as $fuente)
			      	<option value="{{ $fuente->id }}">{{$loop->iteration}}.- {{ $fuente->nombre }}</option>
			      @endforeach
			    </select>
			</div>
        <div class=" card-block col-md-6">
            <a href="{{ route('tarea.index')}}" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-primary">Guardar</button>                    
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection