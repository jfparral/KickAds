@extends('layout.app_layout')
@section('content')
 <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.index")}}">Clientes</a></li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.ver", ["id" => $servicio->id_cliente])}}">Ver</a></li>
        <li class="breadcrumb-item"><a href="{{ route("servicio.editar", ["id" => $servicio->id])}}">Editar servicio</a></li>

        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">
          <form name="create_servicio" method="post" action="{{ route("servicio.actualizar", ["id" => $servicio->id])}}">
            {{ csrf_field() }}
            <div class="form-group">
              <h3 class="card-header">Datos del Servicio</h3>
              <div class="form-group">
                <label for="descripcion">Descripción</label>
                <textarea class="form-control" name="descripcion" rows="3">{{$servicio->descripcion}}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="id_categoria">Seleccionar Categoría:</label>
              <select class="form-control" name="id_categoria">
                @foreach($categorias as $categoria)
                  <option value="{{ $categoria->id }}" @if($categoria->id === $servicio->categoria->id) selected @endif>{{ $categoria->nombre }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-6">
              <a href="{{ route('cliente.ver', ['id' => $servicio->id_cliente])}}" class="btn btn-secondary">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>                    
            </div>
          </form>
        </div>
      </div>
    </div>
  </div> 
</div>
@endsection