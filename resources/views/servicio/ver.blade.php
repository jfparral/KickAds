@extends('layout.app_layout')
@section('content')
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.index")}}">Clientes</a></li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.ver", ["id" => $servicio->id_cliente])}}">Ver</a></li>
        <li class="breadcrumb-item"><a href="{{ route("servicio.ver", ["id" => $servicio->id])}}">Servicio</a></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
             <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>



<div class="container-fluid">
  	<div class="row">
    	<div class="col">
      		<div class="card">
        		<div class="card-block">
						<div class="card-header row">
					    	<div class="col-sm-6">
					    		<h3>Detalles del Servicio/h3>
					    	</div>
					    	<div class="col-sm-6">
						    <div class="col pr-0 d-flex justify-content-end">
							        <a href="{{ route('cliente.ver', [ 'id' => $servicio->id_cliente  ])}}">
							            <button type="button" class="btn btn-secondary">Regresar</button>
							        </a>
							    </div>
							</div>
					    </div>
						  <div class="card-block">
						    <h4 class="card-title">Id del Servicio</h4>
						    <p class="card-text">{{ $servicio->id }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Categoría</h4>
						    <p class="card-text">{{ $servicio->categoria->nombre }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Abreviatura</h4>
						    <p class="card-text">{{ $servicio->categoria->abr }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Descripción</h4>
						    <p class="card-text">{{ $servicio->descripcion }}</p>
						  </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection