@extends('layout.app_layout')
@section('content')
 <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.index")}}">Clientes</a></li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.ver", ["id" => $cliente->id])}}">Ver</a></li>
        <li class="breadcrumb-item"><a href="{{ route("servicio.crear", ["id" => $cliente->id])}}">Agregar servicio</a></li>

        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
          	<a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card-header">
        <form name="create_servicio" method="post" action="{{ route('servicio.guardar', ['id' => $cliente->id])}}">
             	{{ csrf_field() }}
            		<div class="form-group">
            			<h3 class="card-header">Datos del Servicio</h3>
          		    <div class="form-group">
          			    <label for="descripcion">Descripción</label>
          			    <textarea class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
          			  </div>
            		</div>
            		<div class="form-group">
            		    <label for="id_categoria">Seleccionar una categoría:</label>
            		    <select class="form-control" name="id_categoria" id="id_categoria">
            		      @foreach($categorias as $categoria)
            		      	<option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
            		      @endforeach
            		    </select>
            		</div>
            		<div class="col-md-6">
                  <a href="{{ route('cliente.ver', ['id' => $cliente->id])}}" class="btn btn-secondary">Cancelar</a>
                  <button type="submit" class="btn btn-primary">Guardar</button>                    
                </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection