@extends('layout.app_layout')
@section('content')
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route('tarea.index')}}">Tareas</a></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid mb-4">
  <div class="bg-white py-3 row">
    <div class="col-sm-6 d-flex align-items-center">
      <h3 class="mb-0">Tareas</h3>
    </div>
    <div class="col-sm-6 d-flex align-items-center justify-content-end">
      <a href="{{ route('tarea.crear')}}" class="btn btn-success">Agregar Tarea</a>
    </div>
  </div>

    <form method="GET" action="{{ route('tarea.index') }}">
        <div class="form-group">
          <label for="sel1">Filtrar por Cliente:</label>
          <select class="form-control" name="cliente_id" id="sel1">
            <option value=""> Seleccionar cliente</option>
            @foreach($clientes as $cliente)
              <option value="{{$cliente->id}}">{{$loop->iteration}}.- {{$cliente->razon_social}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <button class="btn btn-success">Filtrar</button>
        </div>
    </form>
    <div class="animated fadeIn">
      <div class="row bg-white">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Cliente</th>
              <th scope="col">Tipo</th>
              <th scope="col">Nombre</th>
              <th scope="col">Estado</th>
              <th scope="col">Fecha de Entrega</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($tareas as $tarea)
              <tr>
                <td>{{ $tarea->cliente->razon_social }}</td>
                <td>{{ $tarea->tipo->tipo }} </td>
                <th>{{ $tarea->nombre }}</th>
                <td>{{ $tarea->estado->nombre }}</td>
                <td>{{ $tarea->fecha_entrega }}</td>
                <td>
                  <!-- Button model gestion-->
                  <button type="button" data-toggle="modal" data-target="#gestionarModal{{$tarea->id}}" class="btn btn-primary">Gestionar</button>

                  <!-- Modal Gestion Header-->
                  <div class="modal fade" id="gestionarModal{{$tarea->id}}" tabindex="-1" role="dialog" aria-labelledby="gestionModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                              <h3 class="modal-title" id="gestionModalLabel">Detalles de la Tarea</h3>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            <div class="container-fluid">
                              <div class="row">
                                  <div class="col">
                                    <div class="card-header">
                                      <form name="create_servicio" method="post" action="{{ route('tarea.actualizar', ['id'=>$tarea->id])}}">
                                          {{ csrf_field() }}
                                          <h3 class="card-header">Datos de la Tarea</h3>
                                          <h4 class="card-block">Cliente: {{ $tarea->cliente->razon_social }}</h4>
                                          <div class="card-block">
                                            <h4>Nombre</h4>
                                            <input type="text" name="nombre" value="{{ $tarea->nombre }}" class="form-control" id="inputEmail4" placeholder="nombre">
                                          </div>
                                        <div class="card-block">
                                          <h4>Descripción 
                                            <span class="badge badge-primary"> Versión {{ $tarea->ultimaVersion()->version }}</span>
                                          </h4>
                                          <textarea class="form-control" id="descripcion" name="descripcion" rows="3">{{ $tarea->ultimaVersion()->descripcion }}</textarea>
                                          <input type="text" hidden name="descripcion_id" value="{{ $tarea->ultimaVersion()->id }}">
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class='col-sm-6'>
                                                  <h4>Fecha de Entrega</h4>
                                                    <input type='date' name="fecha_entrega" value="{{ $tarea->fecha_entrega }}" class="form-control" id='datetimepicker4' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <label for="id_categoria">Seleccionar un categoría:</label>
                                            <select class="form-control" name="id_categoria" id="id_categoria">
                                              <option value=""> Seleccionar categoría</option>
                                              @foreach($categorias as $categoria)
                                                <option value="{{ $categoria->id }}" @if($categoria->id === $tarea->id_categoria) selected @endif>{{$loop->iteration}}.- {{ $categoria->nombre }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="card-block">
                                            <label for="id_categoria">Seleccionar un tipo:</label>
                                            <select class="form-control" name="id_tipo" id="id_tipo">
                                              <option value=""> Seleccionar tipo</option>
                                              @foreach($tipos as $tipo)
                                                <option value="{{ $tipo->id }}" @if($tipo->id === $tarea->id_tipo) selected @endif>{{$loop->iteration}}.- {{ $tipo->tipo }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="card-block">
                                            <label for="id_categoria">Seleccionar la fuente:</label>
                                            <select class="form-control" name="id_fuente" id="id_fuente">
                                              <option value=""> Seleccionar fuente</option>
                                              @foreach($fuentes as $fuente)
                                                <option value="{{ $fuente->id }}" @if($fuente->id === $tarea->id_fuente) selected @endif>{{$loop->iteration}}.- {{ $fuente->nombre }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class=" card-block col-md-6">
                                          <div class="card-block">
                                            <h4>Observaciones</h4>
                                            <textarea class="form-control" id="observacion" name="observacion" rows="3">Ingrese una observación</textarea>
                                          </div>
                                          <div class="card-block">
                                              <label for="id_categoria">Seleccionar un estado:</label>
                                              <select class="form-control" name="id_estado" id="id_estado">
                                                <option value=""> Seleccionar estado</option>
                                                @foreach($estados as $estado)
                                                  <option value="{{ $estado->id }}" @if($estado->id === $tarea->id_estado) selected @endif>{{$loop->iteration}}.- {{ $estado->nombre }}</option>
                                                @endforeach
                                              </select>
                                              <div class="card">
                                              <button type="submit" class="btn btn-primary">Actualizar</button>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-header">
                            <h3 class="modal-title" id="gestionModalLabel">Gestiones de la Tarea</h3>
                          </div>
                          <div class="modal-body">
                            <div class="container-fluid">
                              <div class="row">
                                <div class="col">
                                  <div class="card-header">
                                    <div class="animated fadeIn">
                                      <div class="row bg-white">
                                        <table class="table">
                                          <thead class="thead-dark">
                                            <tr>
                                              <th scope="col">Fecha</th>
                                              <th scope="col">Usuario</th>
                                              <th scope="col">Comentario</th>
                                              <th scope="col">Estado</th>
                                              <th scope="col">Acciones</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            @foreach ($tarea->gestiones as $gestion)
                                              <tr>
                                                <td>{{ $gestion->created_at }}</td>
                                                <td>{{ $gestion->user->nombre }} </td>
                                                <th>{{ $gestion->observacion }}</th>
                                                <td>{{ $gestion->gestionable_tipo }}</td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                    <a href="{{ route('tarea.index')}}" class="btn btn-secondary">Cerrar</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div> 
                        </div>
                      </div>
                    </div>
                  <!-- Modal Gestion Body-->
                  
                  <!-- Button model eliminar -->
                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarModal{{$tarea->id}}">
                    Eliminar
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="eliminarModal{{$tarea->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Eliminar Tarea {{ $tarea->nombre }}</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          ¿Está seguro que desea eliminar esta tarea?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                          <a class="btn btn-primary" href="{{ route('tarea.eliminar', [ 'id'=>$tarea->id])}}">
                            Eliminar
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

</div>
@endsection