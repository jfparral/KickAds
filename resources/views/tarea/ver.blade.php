@extends('layout.app_layout')
@section('content')

<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="{{ route("tarea.index")}}">Tareas</a></li>
    <li class="breadcrumb-item"><a href="{{ route("tarea.ver", ["id" => $tarea->id])}}">Ver</a></li>

    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
      <div class="btn-group" role="group" aria-label="Button group">
      	<a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
        <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
        <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
      </div>
    </li>
</ol>

<div class="container-fluid">
  	<div class="row">
    	<div class="col">
      		<div class="card">
        		<div class="card-block">
						<div class="card-header row">
					    	<div class="col-sm-6">
					    		<h3>Detalles de la Tarea</h3>
					    	</div>
					    	<div class="col-sm-6">
						    <div class="col pr-0 d-flex justify-content-end">
							        <a href="{{ route('tarea.index')}}">
							            <button type="button" class="btn btn-secondary">Regresar</button>
							        </a>
							    </div>
							</div>
					    </div>
						  <div class="card-block">
						    <h4 class="card-title">Nombre</h4>
						    <p class="card-text">{{ $tarea->nombre }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Descripción Versión {{ $version->version }}</h4>
						     <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#verModal{{$tarea->id}}">Ver Descripciones
			                </button>

                  				<!-- Modal -->
			                  <div class="modal fade" id="verModal{{$tarea->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			                    <div class="modal-dialog" role="document">
			                      <div class="modal-content">
			                        <div class="modal-header">
			                          <h5 class="modal-title" id="exampleModalLabel">Descripciones</h5>
			                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                            <span aria-hidden="true">&times;</span>
			                          </button>
			                        </div>
			                        <div class="modal-body">
			                          @foreach($descripciones as $descripcion)
			                          	<div class="card-block">
			                          		<div class="card-header">
			                          			Versión
			                          			{{ $descripcion->version}}
			                          		</div>
			                          		<p class="justify">
			                          			{{ $descripcion->descripcion}}
			                          		</p>
			                          	</div>
			                          @endforeach
			                        </div>
			                      </div>
			                    </div>
			                  </div>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Fecha de Entrega</h4>
						    <p class="card-text">{{ $tarea->fecha_entrega }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Cliente</h4>
						    <p class="card-text">{{ $tarea->cliente->razon_social }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Usuario</h4>
						    <p class="card-text">{{ $tarea->user->nombre }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Categoría</h4>
						    <p class="card-text">{{ $tarea->categoria->nombre }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Abreviatura de la Categoía</h4>
						    <p class="card-text">{{ $tarea->categoria->abr }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Estado</h4>
						    <p class="card-text">{{ $tarea->estado->tipo }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Tipo</h4>
						    <p class="card-text">{{ $tarea->tipo->tipo }}</p>
						  </div>
						  <div class="card-block">
						    <h4 class="card-title">Fuente</h4>
						    <p class="card-text">{{ $tarea->fuente->nombre}}</p>
						  </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection