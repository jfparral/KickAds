@extends('layout.app_layout')
@section('content')
      <div class="container-fluid">
        <div class="row mb-4">
          <div class="col pr-0 d-flex justify-content-end">
            <a href="http://127.0.0.1:8000/cliente/crear">
                <button type="button" class="btn btn-success pull-right">Agregar Cliente</button>
            </a>
          </div>
        </div>
        <div class="animated fadeIn">
          <div class="row">
                  <table class="table">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Logo</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($clientes as $cliente)
                            <tr>
                              <th scope="row"> {{ $cliente->id }} </th>
                              <td>null</td>
                              <td>{{ $cliente->razon_social }}</td>
                              <td>{{ $cliente->telefono }}</td>
                              <td>
                                <button type="button" class="btn btn-primary">Ver</button>
                                <a href="http://127.0.0.1:8000/cliente/editar">
                                    <button type="button" class="btn btn-info">Editar</button>
                                </a>
                                <button type="button" class="btn btn-danger">Eliminar</button>
                              </td>
                            </tr>
                      @endforeach
                    </tbody>
                  </table>
          </div>
@endsection
