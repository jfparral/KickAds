<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.10
 * @link http://coreui.io
 * Copyright (c) 2018 creativeLabs Łukasz Holeczek
 * @license MIT
 -->

<!DOCTYPE html>
<html lang="es">
<!-- Head -->
@include('layout.head')
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  @include('layout.app-hearder')
  <div class="app-body " >
    <!-- Siderbar -->
    @include('layout.siderbar')

    <!-- Main content -->
    <main class="main">

      <!-- Tabla de clientes-->
      @yield('content')

    </main>
  </div>
    @include('layout.footer')
    
    <!-- Bootstrap and necessary plugins -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

    <!-- Plugins and scripts required by all views -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

    <!-- CoreUI main scripts -->

    <script src="js/app.js"></script>

    <!-- Plugins and scripts required by this views -->

    <!-- Custom scripts required by this view -->
    <script src="js/views/main.js"></script>
</body>
</html>