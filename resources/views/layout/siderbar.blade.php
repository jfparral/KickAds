<div class="sidebar">
<nav class="sidebar-nav">
        <ul class="nav">
          <li class="nav-title">
            Clientes
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("cliente.index")}}"><i class="icon-user"> </i> Clientes </a>
          </li>

          <li class="nav-title">
            Tareas
          </li>
          <li class="nav-item">
            <a href="{{ route('solicitud.index')}}" class="nav-link"><i class="icon-doc"></i> Solicitudes</a>
          </li>
          <li class="nav-item">
            <a href="{{ route('tarea.index')}}" class="nav-link"><i class="icon-notebook"></i> Tareas</a>
          </li>
        </ul>
      </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>