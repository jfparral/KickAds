@extends('layout.app_layout')
@section('content')
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route('cliente.index')}}">Clientes</a></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
      <div class="container-fluid mb-4">
        <div class="bg-white py-3 row">
          <div class="col-sm-6 d-flex align-items-center">
            <h3 class="mb-0">Clientes</h3>
          </div>
          <div class="col-sm-6 d-flex align-items-center justify-content-end">
            <a href="{{ route('cliente.crear')}}" class="btn btn-success">Agregar cliente</a>
          </div>
        </div>
        <div class="animated fadeIn">
          <div class="row bg-white">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Logo</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Teléfono</th>
                  <th scope="col">Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($clientes as $cliente)
                      <tr>
                        <th scope="row">{{$loop->iteration}} </th>
                        <td>
                          <img src="{{$cliente->logo}}" alt="" width="75">
                        </td>
                        <td>{{ $cliente->razon_social }}</td>
                        <td>{{ $cliente->telefono }}</td>
                        <td>
                          <a href="http://127.0.0.1:8000/cliente/ver/{{$cliente->id}}">
                              <button type="button" class="btn btn-primary">Ver</button>
                          </a>
                          <a href="http://127.0.0.1:8000/cliente/editar/{{$cliente->id}}">
                              <button type="button" class="btn btn-info">Editar</button>
                          </a>
                          
                          <!-- Button trigger modal -->
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarModal{{$cliente->id}}">
                            Eliminar
                          </button>

                          <!-- Modal -->
                          <div class="modal fade" id="eliminarModal{{$cliente->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente {{ $cliente->razon_social }}</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  ¿Está seguro que desea eliminar este cliente?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                  <a class="btn btn-primary" href="{{ route('cliente.eliminar', [ 'id'=>$cliente->id])}}">
                                    Eliminar
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
@endsection