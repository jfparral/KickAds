@extends('layout.app_layout')
@section('content')
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.index")}}">Clientes</a></li>
        <li class="breadcrumb-item"><a href="{{ route("cliente.ver", ["id" => $cliente->id])}}">Ver</li>
        <!-- Breadcrumb Menu-->
 		<li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-block">
					<div class="card-header row">
				    	<div class="col-sm-6">
				    		<h3>Detalles del Cliente</h3>
				    	</div>
				    	<div class="col-sm-6">
					    	<div class="col pr-0 d-flex justify-content-end">
						        <a href="{{ route('cliente.index')}}">
						            <button type="button" class="btn btn-secondary">Regresar</button>
						        </a>
						    </div>
						</div>
				    </div>
					  <div class="card-block">
					    <h4 class="card-title">Id del Cliente</h4>
					    <p class="card-text">{{ $cliente->id }}</p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">Razón Social</h4>
					    <p class="card-text">{{ $cliente->razon_social }}</p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">Telefono</h4>
					    <p class="card-text">{{ $cliente->telefono }}</p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">RUC</h4>
					    <p class="card-text">{{ $cliente->ruc }}</p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">Dirección</h4>
					    <p class="card-text">{{$cliente->direccion}}</p>
					  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-block">
				    <div class="card-header row">
				    	<div class="col-sm-6">
				    		<h3>Servicios</h3>
				    	</div>
				    	<div class="col-sm-6">
					    <div class="col pr-0 d-flex justify-content-end">
						        <a href="{{ route('servicio.crear', [ 'id' => $cliente->id  ])}}">
						            <button type="button" class="btn btn-success pull-right">Agregar Servicio</button>
						        </a>
						    </div>
						</div>
				    </div>

				    <div class="card-block">
				    	   <div class="animated fadeIn">
					          <div class="row">
			                  	<table class="table">
			                    <thead class="thead-dark">
			                      <tr>
			                        <th scope="col">#</th>
			                        <th scope="col">Categoría</th>
			                        <th scope="col">Acciones</th>
			                      </tr>
			                    </thead>
		                    <tbody>
							    @foreach($cliente->servicios as $servicio)
						                <tr>
						                  <th scope="row"> {{ $servicio->id }} </th>
						                  <td>{{ $servicio->categoria->nombre }}</td>
						                  <td>
						                    <a href="{{ route('servicio.ver', [ 'id'=>$servicio->id])}}">
						                        <button type="button" class="btn btn-primary">Ver</button>
						                    </a>
						                    <a href="{{ route('servicio.editar', [ 'id'=>$servicio->id])}}">
						                        <button type="button" class="btn btn-info">Editar</button>
						                    </a>
						                    
						                    <!-- Button trigger modal -->
						                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarModal{{$servicio->id}}">
						                      Eliminar
						                    </button>

						                    <!-- Modal -->
						                    <div class="modal fade" id="eliminarModal{{$servicio->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						                      <div class="modal-dialog" role="document">
						                        <div class="modal-content">
						                          <div class="modal-header">
						                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Servicio</h5>
						                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                              <span aria-hidden="true">&times;</span>
						                            </button>
						                          </div>
						                          <div class="modal-body">
						                            ¿Está seguro que desea eliminar este servicio?
						                          </div>
						                          <div class="modal-footer">
						                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
						                            <a class="btn btn-primary" href="{{ route('servicio.eliminar', [ 'id'=>$servicio->id])}}">
						                              Eliminar 
						                            </a>
						                          </div>
						                        </div>
						                      </div>
						                    </div>
						                  </td>
						                </tr>
						        @endforeach
						    </tbody>
                 		</table>
         			</div>
				   </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
