<?php

use Faker\Generator as Faker;

$factory->define(App\User_cliente::class, function (Faker $faker) {
    return [
        'user_id'=>function(){
        	return factory(App\User::class)->create()->id;
        },
        'cliente_id'=>function(){
        	return factory(App\Cliente::class)->create()->id;
        }
    ];
});
