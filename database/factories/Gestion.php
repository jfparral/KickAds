<?php

use Faker\Generator as Faker;

$factory->define(App\Gestion::class, function (Faker $faker) {
    return [
        'id_user'=>function(){
        	return factory(App\User::class)->create()->id;
        },
		'id_estado'=>function(){
        	return factory(App\Estado::class)->create()->id;
        },
		'id_tarea'=>function(){
        	return factory(App\Tarea::class)->create()->id;
        },
        'gestionable_tipo'=>$faker->text($maxNbChars = 20),
        'observacion'=>$faker->text($maxNbChars = 100)
    ];
});
