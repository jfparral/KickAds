<?php

use Faker\Generator as Faker;

$factory->define(App\Descripcion::class, function (Faker $faker) {
    return [
        'descripcion'=>$faker->text($maxNbChars = 200),
        'version'=>$faker->randomDigit,
        'id_tarea'=>function(){
        	return factory(App\Tarea::class)->create()->id;
        }
    ];
});
