<?php

use Faker\Generator as Faker;

$factory->define(App\Tarea::class, function (Faker $faker) {
    return [
        'nombre'=>$faker->word,
        'fecha_entrega'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'id_cliente'=>function(){
        	return factory(App\Cliente::class)->create()->id;
        },
        'id_user'=>function(){
        	return factory(App\User::class)->create()->id;
        },
        'id_estado'=>function(){
        	return factory(App\Estado::class)->create()->id;
        },
        'id_categoria'=>function(){
        	return factory(App\Categoria::class)->create()->id;
        },
        'id_tipo'=>function(){
        	return factory(App\Tipo::class)->create()->id;
        },
        'id_fuente'=>function(){
        	return factory(App\Fuente::class)->create()->id;
        }
        //$pivot=>function(App\User_cliente::class)->create()->id;
    ];
});
