<?php

use Faker\Generator as Faker;

$factory->define(App\Tipo::class, function (Faker $faker) {
    return [
        'tipo'=>$faker->text($maxNbChars = 20) 
    ];
});
