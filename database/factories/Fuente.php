<?php

use Faker\Generator as Faker;

$factory->define(App\Fuente::class, function (Faker $faker) {
    return [
        'nombre'=>$faker->firstNameMale
    ];
});
