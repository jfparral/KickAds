<?php

use Faker\Generator as Faker;

$factory->define(App\Cliente::class, function (Faker $faker) {
    return [
        'razon_social'=>$faker->name,
        'direccion'=>$faker->address,
        'ruc'=>$faker->ean13,
        'telefono'=>$faker->phoneNumber
    ];
});
