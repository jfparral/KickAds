<?php

use Faker\Generator as Faker;

$factory->define(App\Servicio::class, function (Faker $faker) {
    return [
        'descripcion'=>$faker->text($maxNbChars = 200),
        'id_cliente'=>function(){
        	return factory(App\Cliente::class)->create()->id;
        },
        'id_categoria'=>function(){
        	return factory(App\Categoria::class)->create()->id;
        }
    ];
});
