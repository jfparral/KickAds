<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestiones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_estado')->unsigned()->nullable();
            $table->text('observacion')->nullable();
            $table->string('gestionable_tipo');
            $table->integer('id_tarea')->unsigned();
            $table->integer('id_user')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('id_estado')
                ->references('id')->on('estados')
                ->onDelete('set null');
            $table->foreign('id_tarea')
                ->references('id')->on('tareas')
                ->onDelete('cascade');
            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
