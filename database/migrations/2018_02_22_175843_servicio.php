<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Servicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('id_categoria')->unsigned()->nullable();
            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_categoria')
                ->references('id')->on('categorias')
                ->onDelete('set null');
            $table->foreign('id_cliente')
                ->references('id')->on('clientes')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
