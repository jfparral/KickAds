<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tareas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tareas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->date('fecha_entrega');
            $table->integer('id_user')->unsigned();
            $table->integer('id_cliente')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('id_tipo')->unsigned();
            $table->integer('id_categoria')->unsigned();
            $table->integer('id_fuente')->unsigned();
            $table->integer('id_solicitud')->unsigned();
            $table->foreign('id_user')
                ->references('id')->on('users');
            $table->foreign('id_cliente')
                ->references('id')->on('clientes')
                ->onDelete('cascade');
            $table->foreign('id_estado')
                ->references('id')->on('estados');
            $table->foreign('id_tipo')
                ->references('id')->on('tipos');
            $table->foreign('id_categoria')
                ->references('id')->on('categorias');
            $table->foreign('id_fuente')
                ->references('id')->on('fuentes');
            $table->foreign('id_solicitud')
                ->references('id')->on('solicitudes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tareas');
    }
}
