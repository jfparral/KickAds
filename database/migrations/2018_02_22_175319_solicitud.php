<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Solicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_cliente')
                ->references('id')->on('clientes');
            $table->integer('id_user-cliente')->unsigned();
            $table->foreign('id_user-cliente')
                ->references('id')->on('users');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')
                ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
