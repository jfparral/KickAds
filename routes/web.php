<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Clientes Route
Route::get('/cliente', 'ClientesController@cliente_index')->name('cliente.index');
Route::get('/cliente/crear', 'ClientesController@create')->name('cliente.crear');
Route::get('/cliente/editar/{id}', 'ClientesController@editar')->name('cliente.editar');
Route::post('/cliente/actualizar/{id}', 'ClientesController@actualizar')->name('cliente.actualizar');
Route::post('/cliente/guardar', 'ClientesController@guardar')->name('cliente.guardar');
Route::get('/cliente/ver/{id}','ClientesController@ver')->name("cliente.ver");
Route::get('/cliente/eliminar/{id}','ClientesController@eliminar')->name('cliente.eliminar');

//Solictudes Route
Route::get('/solicitud','SolicitudesController@index')->name('solicitud.index');
Route::get('/solicitud/crear','SolicitudesController@crear')->name('solicitud.crear');
Route::get('/solicitud/eliminar/{id}','SolicitudesController@eliminar')->name('solicitud.eliminar');

//Servicios Route
Route::get('/servicio/ver/{id}','ServiciosController@ver')->name('servicio.ver');
Route::get('/servicio/crear/{id}','ServiciosController@crear')->name('servicio.crear');
Route::post('/servicio/guardar/{id}','ServiciosController@guardar')->name('servicio.guardar');
Route::get('/servicio/editar/{id}','ServiciosController@editar')->name('servicio.editar');
Route::post('/servicio/actualizar/{id}','ServiciosController@actualizar')->name('servicio.actualizar');
Route::get('/servicio/eliminar/{id}','ServiciosController@eliminar')->name('servicio.eliminar');

//Tareas Route
Route::get('/tarea', 'TareasController@index')->name('tarea.index');
Route::get('/tarea/crear','TareasController@crear')->name('tarea.crear');
Route::post('/tarea/guardar','TareasController@guardar')->name('tarea.guardar');
Route::get('/tarea/ver/{id}', 'TareasController@ver')->name('tarea.ver');
Route::get('/tarea/gestionar/{id}', 'TareasController@gestionar')->name('tarea.gestionar');
Route::post('/tarea/actualizar/{id}','TareasController@actualizar')->name('tarea.actualizar');
Route::get('/tarea/eliminar/{id}','TareasController@eliminar')->name('tarea.eliminar');


