<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{
    protected $table='gestiones';

    protected $fillable = ['id_estado','descripcion','gestionable_tipo','id_tarea', 'id_user'];

    protected $with = ['user', 'estado'];

    public function user()
    {
    	return $this->belongsTo('App\User','id');
    }

    public function estado()
    {
    	return $this->belongsTo('App\Estado','id');
    }
    public function tarea()
    {
        return $this->belongsTo('App\Tarea','id');
    }


}
