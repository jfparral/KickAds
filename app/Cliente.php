<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public $timestamps=false;

    protected $guarded = ['logo'];
    protected $with = ['contactos'];
    protected $fillable = ['razon_social','ruc','direccion','telefono']; // Mass asingment 
 
      public function users()
    {
        return $this->belongsToMany('\App\User','user_clientes')
        ->withPivot('user_id');
    }

  	public function servicios()
  	{
  		return $this->hasMany('App\Servicio','id_cliente');
  	}

    public function tareas()
    {
      return $this->hasMany('App\Tarea','id_cliente');
    }

    public function contactos()
    {
      return $this->hasMany('App\User','id_cliente');
    }

    public function getLogoAttribute($value)
    {
            return asset("storage/{$value}");
    }

}













