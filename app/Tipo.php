<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    public $timestamps=false;

    public function tareas()
    {
      return $this->hasMany('App\Tarea','id_cliente');
    }
}
