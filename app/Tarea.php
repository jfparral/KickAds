<?php

namespace App;
use App\Descripcion;
use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $fillable = ['nombre','fecha_entrega','id_cliente','id_estado', 'id_fuente', 'id_tipo', 'id_categoria', 'id_user'];
    protected $with = ['categoria', 'cliente', 'user', 'estado', 'tipo', 'fuente','descripciones', 'gestiones'];

    public function categoria()
    {
    	return $this->belongsTo(Categoria::class,'id_categoria');
    }

    public function cliente()
    {
    	return $this->belongsTo(Cliente::class,'id_cliente');
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'id_user'); 	
    }

    public function tipo()
    {
    	return $this->belongsTo(Tipo::class,'id_tipo');
    }

    public function fuente()
    {
    	return $this->belongsTo(Fuente::class,'id_fuente');
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class,'id_estado');
    }

    public function descripciones()
    {
        return $this->hasMany(Descripcion::class,'id_tarea');
    }

    public function ultimaVersion()
    {
        return $this->descripciones->sortByDesc('version')->first();
    }
    public function gestiones()
    {
        return $this->hasMany(Gestion::class,'id_tarea');
    }
}
