<?php

namespace App;

use App\Categoria;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
	protected $table ='solicitudes';
    protected $guarded = [];

    protected $with = ['tarea', 'cliente', 'contacto', 'descripcione', 'estado'];

    public function tarea()
    {
    	return $this->hasMany(Tareas::class, 'id_solicitud');
    }

    public function cliente()
    {
    	return $this->belongsTo(Cliente::class,'id');
    }

    public function contacto()
    {
    	return $this->belongsTo(User::class, 'id');
    }

    public function descripcione()
    {
    	return $this->belongsTo(Descripcion::class,'id');
    }

    public function estado()
    {
    	return $this->belongsTo(Estados::class,'id');
    }

}
