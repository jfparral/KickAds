<?php

namespace App;

use App\Categoria;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $guarded = [];

    protected $with = ['categoria'];

    public function categoria()
    {
    	return $this->belongsTo(Categoria::class, 'id_categoria');
    }
}
