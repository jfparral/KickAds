<?php

namespace App\Http\Controllers;

use App\Servicio, App\Cliente, App\Categoria;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{

	public function crear($id)
	{
		$cliente = Cliente::find($id);
		$categorias = Categoria::all();
		return view('servicio.crear', compact('cliente', 'categorias'));
	}

	public function ver($id)
	{
		
		$servicio = Servicio::find($id);
		return view('servicio.ver',compact('servicio'));
	}

	public function editar($id)
	{
		$servicio = Servicio::find($id);
		$categorias = Categoria::all();
		return view('servicio.editar',compact('servicio','categorias'));
	}

	public function guardar(Request $request, $id)
	{

		$request->validate([
			'descripcion' => 'required',
			'id_categoria' => 'required|exists:categorias,id'
		]);

		$cliente = Cliente::find($id);

		$cliente->servicios()->save(new Servicio($request->all()));

		return redirect()->route("cliente.ver", ["id" => $id])->with('success', "El servicio ha sido creado correctamente.");
	}

	public function actualizar(Request $request, $id)
	{
		$request->validate([
			'descripcion' => 'required',
			'id_categoria' => 'required|exists:categorias,id'
		]);

		

		$servicio = Servicio::find($id);


		$servicio->descripcion=$request->input('descripcion');
		$servicio->id_categoria=$request->input('id_categoria');
		$servicio->save();
		
		return redirect()->route("servicio.ver",["id" => $id])->with('success', "El servicio ha sido actualizado correctamente.");
	}

	public function eliminar($id)
	{
		$servicio = Servicio::find($id);
		$id_cliente=$servicio->id_cliente;
		$servicio->delete();
		
		return redirect()->route("cliente.ver",["id" => $id_cliente])->with('success', "El servicio ha sido eliminado correctamente.");
	}
}
