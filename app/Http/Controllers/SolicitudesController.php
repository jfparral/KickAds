<?php

namespace App\Http\Controllers;
use App\Solicitud, App\Tarea, App\Cliente, App\Estado, App\Fuente, App\User, App\Descripcion;
use Illuminate\Http\Request;

class SolicitudesController extends Controller
{
    public function index(Request $request)
    {
    	$clientes=Cliente::all();
    	$solicitudes=Solicitud::when($request->filled('cliente_id'), function($query) use($request) {
    						return $query->where('id_cliente', $request->input('cliente_id'));
    					})->get();
    	return view('solicitud.index',compact('solicitudes','clientes'));
    }

    public function crear(Request $request)
    {
    	$clientes=Cliente::all();
    	$contactos=User::when($request->filled('cliente_id'), function($query) use($request) {
    						return $query->where('id_cliente', $request->input('cliente_id'));
    					})->get();
    	$fuente=Fuente::all();
    	return view('solicitud.crear',compact('clientes','contactos','fuente'));
    }

    public function guardar(Request $request)
    {
    	$request->validate([
    		'id_cliente' => 'required',
    		'id_usuario' => 'required',
    		'descripcion' => 'required',
    		'fuente' => 'required'
    	]);
    }

    public function eliminar($id)
    {
    	$solicitud=Solicitud::find($id);
    	$solicitud->delete();
    	return redirect()->route('solicitd.index')->with('success','La solicitdha sido eliminada exitosamente.');
    }
}
