<?php

namespace App\Http\Controllers;
use App\Tarea, App\Cliente, App\Estado, App\Categoria, App\Fuente, App\User, App\Tipo, App\Descripcion, App\Gestion;
use Illuminate\Http\Request;

class TareasController extends Controller
{
    public function index(Request $request)
    {
    	$clientes = Cliente::all();
    	$tareas = Tarea::when($request->filled('cliente_id'), function($query) use($request) {
    						return $query->where('id_cliente', $request->input('cliente_id'));
    					})->get();
        $tipos=Tipo::all();
        $estados=Estado::all();
        $categorias=Categoria::all();
        $fuentes=Fuente::all();
        $gestiones=Gestion::all();
    	return view('tarea.index', compact('tareas','clientes', 'tipos', 'estados', 'fuentes', 'categorias','gestiones'));
    }

    public function crear()
    {
    	$clientes=Cliente::all();
    	$tipos=Tipo::all();
    	$categorias=Categoria::all();
    	$fuentes=Fuente::all();
    	$users=User::all();
    	return view('tarea.crear', compact('clientes', 'tipos', 'fuentes', 'categorias', 'users'));
    }

    public function guardar(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'fecha_entrega' => 'required',
            'id_categoria' => 'required|exists:categorias,id',
            'id_cliente' => 'required|exists:clientes,id',
            'id_user' => 'required|exists:users,id',
            'id_tipo' => 'required|exists:tipos,id',
            'id_fuente' => 'required|exists:fuentes,id'
        ]);

        $tarea= new Tarea($request->all());
        $tarea->id_estado=1;
        $tarea->save();       

        $descripcion = new Descripcion($request->only('descripcion'));
        $descripcion->id_tarea = $tarea->id;
        $descripcion->version= 1;
        $descripcion->save();

        $gestion = new Gestion();
        $gestion->id_tarea=$tarea->id;
        $gestion->id_estado=$tarea->id_estado;
        $estado=Estado::find(1);
        $gestion->gestionable_tipo=$estado->nombre;
        $gestion->save();

        //$tarea->descripciones()->save(new Descripcion($request->only('descripcion'));

    	return redirect()->route("tarea.index")->with('success', "La tarea ha sido creada correctamente.");
    }

/*    public function ver($id)
    {
        $tarea=Tarea::find($id);
        $descripciones= Descripcion::where('id_tarea', $id)->orderBy('version', 'desc')->get();
        $version = Descripcion::where('id_tarea', $id)->orderBy('version', 'desc')->first();
        return view('tarea.ver',compact('tarea', 'descripciones','version'));
    }

    public function gestionar($id)
    {
        $tarea=Tarea::find($id);
        $descripcion=Descripcion::where('id_tarea', $id)->orderBy('version', 'desc')->first();
        $cliente=Cliente::where('id', $tarea->id_cliente)->first();
        $tipos=Tipo::all();
        $estados=Estado::all();
        $categorias=Categoria::all();
        $fuentes=Fuente::all();
        $users=User::all();
        return view('tarea.editar',compact('tarea', 'descripcion','cliente', 'tipos', 'estados', 'fuentes', 'categorias', 'users'));
    }*/

    public function actualizar(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'observacion' => 'required',
            'fecha_entrega' => 'required',
            'id_categoria' => 'required|exists:categorias,id',
            'id_estado' => 'required|exists:estados,id',
            'id_tipo' => 'required|exists:tipos,id',
            'id_fuente' => 'required|exists:fuentes,id',
            'descripcion_id' => 'required'
        ]);

        $tarea=Tarea::find($id);
        if($tarea->id_estado != $request->input('id_estado'))
        {
            $gestion = new Gestion();
            $gestion->id_tarea=$tarea->id;
            $gestion->id_estado=$request->input('id_estado');
            $gestion->observacion=$request->input('observacion');
            $estado=Estado::find($request->input('id_estado'));
            $gestion->gestionable_tipo=$estado->nombre;
            $gestion->save();
        }
        $tarea->update($request->only(['nombre', 'fecha_entrega', 'id_categoria', 'id_estado', 'id_fuente', 'id_tipo']));

        $descripcion = Descripcion::find($request->input('descripcion_id'));

        if($descripcion->descripcion != $request->input('descripcion')) {
            Descripcion::create([
                'descripcion' => $request->input('descripcion'),
                'version' => $descripcion->version + 1,
                'id_tarea' => $tarea->id
            ]);
        }

        return redirect()->route('tarea.index')->with('success','La tarea ha sido actualizada correctamente.');
    }


    public function eliminar($id)
    {
        $tarea=Tarea::find($id);
        $descripcion=Descripcion::find();
        $descripcion->delete();
        $tarea->delete();
        return redirect()->route('tarea.index')->with('success','Tarea eliminada con éxito.');  
    }

}
