<?php


namespace App\Http\Controllers;
use App\Cliente, App\Servicio, App\Categoria;
use Illuminate\Http\Request;


class ClientesController extends Controller
{
    

	public function cliente_index()
	{
		$clientes=Cliente::with(['servicios'])->get();
    	return view('cliente.index',compact('clientes'));
	}



	public function create()
	{
		return view('cliente.crear');
	}

	public function ver($id)
	{
		
		$cliente = Cliente::with('servicios')->find($id);

		return view('cliente.ver',compact(['cliente']));
	}

	public function editar($id)
	{
		$cliente = Cliente::find($id);
		return view('cliente.editar',compact('cliente'));
	}

	public function servicio($id)
	{
		$cliente = Cliente::find($id);
		return view('cliente.servicio',compact('cliente'));
	}

	public function guardar(Request $request)
	{

		$request->validate([
			'telefono'=>'required',
			'ruc'=>'required|min:13|max:13',
			'razon_social'=>'required',
			'direccion'=>'required'
		]);
		$cliente = new Cliente($request->except(['logo']));

		if($request->hasFile('logo'))
		{
			$cliente->logo=$request->file('logo')->store('logos');
		}

		$cliente->save();
		
		return redirect()->action('ClientesController@cliente_index')->with('success', "El cliente {$cliente->razon_social} ha sido creado correctamente.");
	}

	public function actualizar(Request $request, $id)
	{
		$request->validate([
			'telefono'=>'required',
			'ruc'=>'required|min:13|max:13',
			'razon_social'=>'required',
			'direccion'=>'required'
		]);

		

		$cliente = Cliente::find($id);
		$cliente->telefono=$request->input('telefono');
		$cliente->razon_social=$request->input('razon_social');
		$cliente->ruc=$request->input('ruc');
		$cliente->direccion=$request->input('direccion');
		if($request->hasFile('logo'))
		{
			$cliente->logo=$request->file('logo')->store('logos');
		}
		$cliente->save();
		
		return redirect()->action('ClientesController@cliente_index')->with('success', "El cliente {$cliente->razon_social} ha sido actualizado correctamente.");
	}

	public function eliminar($id)
	{
		$cliente = Cliente::find($id);
		$cliente->delete();
		
		return redirect()->action('ClientesController@cliente_index')->with('success', "El cliente ha sido eliminado correctamente.");
	}


}
