<?php $__env->startSection('content'); ?>
 <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="http://127.0.0.1:8000/cliente">Cliente</a></li>
        <li class="breadcrumb-item"><a href="http://127.0.0.1:8000/cliente/ver/<?php echo e($cliente->id); ?>">ver</li>
        	        <li class="breadcrumb-item"><a href="http://127.0.0.1:8000/cliente/ver/<?php echo e($cliente->id); ?>/servicio">agregar servicio</li>

        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i></a>
            <a class="btn" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Settings</a>
          </div>
        </li>
      </ol>
     <form name="create_servicio" method="post" action="<?php echo e(route("cliente.servicio", [ "id" => $cliente->id ])); ?>" enctype="multipart/form-data">
		<div class="form-group">
			<h3 class="card-header">Datos del Servicio</h3>
		    <label for="exampleInputEmail1">Descripción del Servicio</label>
		    <input type="email" class="form-control" name="descripcion" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese descripción">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Categoría </label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Ingrese categoría">
		  </div>
		  <div class="form-check">
		    <input type="checkbox" class="form-check-input" id="exampleCheck1">
		    <label class="form-check-label" for="exampleCheck1">Check me out</label>
		</div>
		  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app_layoyt', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>