<?php $__env->startSection('content'); ?>
 <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.index")); ?>">Clientes</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.ver", ["id" => $servicio->id_cliente])); ?>">Ver</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("servicio.editar", ["id" => $servicio->id])); ?>">Editar servicio</a></li>

        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">
          <form name="create_servicio" method="post" action="<?php echo e(route("servicio.actualizar", ["id" => $servicio->id])); ?>">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
              <h3 class="card-header">Datos del Servicio</h3>
              <div class="form-group">
                <label for="descripcion">Descripción</label>
                <textarea class="form-control" name="descripcion" rows="3"><?php echo e($servicio->descripcion); ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="id_categoria">Seleccionar Categoría:</label>
              <select class="form-control" name="id_categoria">
                <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($categoria->id); ?>" <?php if($categoria->id === $servicio->categoria->id): ?> selected <?php endif; ?>><?php echo e($categoria->nombre); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>
            <div class="col-md-6">
              <a href="<?php echo e(route('cliente.ver', ['id' => $servicio->id_cliente])); ?>" class="btn btn-secondary">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>                    
            </div>
          </form>
        </div>
      </div>
    </div>
  </div> 
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>