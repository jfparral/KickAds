<?php $__env->startSection('content'); ?>
      <div class="container-fluid">
        <div class="row mb-4">
          <div class="col pr-0 d-flex justify-content-end">
            <a href="http://127.0.0.1:8000/cliente/crear">
                <button type="button" class="btn btn-success pull-right">Agregar Cliente</button>
            </a>
          </div>
        </div>
        <div class="animated fadeIn">
          <div class="row">
                  <table class="table">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Logo</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cliente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                              <th scope="row"> <?php echo e($cliente->id); ?> </th>
                              <td>null</td>
                              <td><?php echo e($cliente->razon_social); ?></td>
                              <td><?php echo e($cliente->telefono); ?></td>
                              <td>
                                <button type="button" class="btn btn-primary">Ver</button>
                                <a href="http://127.0.0.1:8000/cliente/editar">
                                    <button type="button" class="btn btn-info">Editar</button>
                                </a>
                                <button type="button" class="btn btn-danger">Eliminar</button>
                              </td>
                            </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
          </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>