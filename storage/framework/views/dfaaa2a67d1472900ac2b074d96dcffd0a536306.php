<?php $__env->startSection('content'); ?>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.index")); ?>">Clientes</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.ver", ["id" => $cliente->id])); ?>">Ver</li>
        <!-- Breadcrumb Menu-->
 		<li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-block">
					<div class="card-header row">
				    	<div class="col-sm-6">
				    		<h3>Detalles del Cliente</h3>
				    	</div>
				    	<div class="col-sm-6">
					    	<div class="col pr-0 d-flex justify-content-end">
						        <a href="<?php echo e(route('cliente.index')); ?>">
						            <button type="button" class="btn btn-secondary">Regresar</button>
						        </a>
						    </div>
						</div>
				    </div>
					  <div class="card-block">
					    <h4 class="card-title">Id del Cliente</h4>
					    <p class="card-text"><?php echo e($cliente->id); ?></p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">Razón Social</h4>
					    <p class="card-text"><?php echo e($cliente->razon_social); ?></p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">Telefono</h4>
					    <p class="card-text"><?php echo e($cliente->telefono); ?></p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">RUC</h4>
					    <p class="card-text"><?php echo e($cliente->ruc); ?></p>
					  </div>
					  <div class="card-block">
					    <h4 class="card-title">Dirección</h4>
					    <p class="card-text"><?php echo e($cliente->direccion); ?></p>
					  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-block">
				    <div class="card-header row">
				    	<div class="col-sm-6">
				    		<h3>Servicios</h3>
				    	</div>
				    	<div class="col-sm-6">
					    <div class="col pr-0 d-flex justify-content-end">
						        <a href="<?php echo e(route('servicio.crear', [ 'id' => $cliente->id  ])); ?>">
						            <button type="button" class="btn btn-success pull-right">Agregar Servicio</button>
						        </a>
						    </div>
						</div>
				    </div>

				    <div class="card-block">
				    	   <div class="animated fadeIn">
					          <div class="row">
			                  	<table class="table">
			                    <thead class="thead-dark">
			                      <tr>
			                        <th scope="col">#</th>
			                        <th scope="col">Categoría</th>
			                        <th scope="col">Acciones</th>
			                      </tr>
			                    </thead>
		                    <tbody>
							    <?php $__currentLoopData = $cliente->servicios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $servicio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						                <tr>
						                  <th scope="row"> <?php echo e($servicio->id); ?> </th>
						                  <td><?php echo e($servicio->categoria->nombre); ?></td>
						                  <td>
						                    <a href="<?php echo e(route('servicio.ver', [ 'id'=>$servicio->id])); ?>">
						                        <button type="button" class="btn btn-primary">Ver</button>
						                    </a>
						                    <a href="<?php echo e(route('servicio.editar', [ 'id'=>$servicio->id])); ?>">
						                        <button type="button" class="btn btn-info">Editar</button>
						                    </a>
						                    
						                    <!-- Button trigger modal -->
						                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarModal<?php echo e($servicio->id); ?>">
						                      Eliminar
						                    </button>

						                    <!-- Modal -->
						                    <div class="modal fade" id="eliminarModal<?php echo e($servicio->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						                      <div class="modal-dialog" role="document">
						                        <div class="modal-content">
						                          <div class="modal-header">
						                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Servicio</h5>
						                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                              <span aria-hidden="true">&times;</span>
						                            </button>
						                          </div>
						                          <div class="modal-body">
						                            ¿Está seguro que desea eliminar este servicio?
						                          </div>
						                          <div class="modal-footer">
						                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
						                            <a class="btn btn-primary" href="<?php echo e(route('servicio.eliminar', [ 'id'=>$servicio->id])); ?>">
						                              Eliminar 
						                            </a>
						                          </div>
						                        </div>
						                      </div>
						                    </div>
						                  </td>
						                </tr>
						        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						    </tbody>
                 		</table>
         			</div>
				   </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>