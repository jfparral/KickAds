<?php $__env->startSection('content'); ?>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('tarea.index')); ?>">Tareas</a></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid mb-4">
  <div class="bg-white py-3 row">
    <div class="col-sm-6 d-flex align-items-center">
      <h3 class="mb-0">Tareas</h3>
    </div>
    <div class="col-sm-6 d-flex align-items-center justify-content-end">
      <a href="<?php echo e(route('tarea.crear')); ?>" class="btn btn-success">Agregar Tarea</a>
    </div>
  </div>

    <form method="GET" action="<?php echo e(route('tarea.index')); ?>">
        <div class="form-group">
          <label for="sel1">Filtrar por Cliente:</label>
          <select class="form-control" name="cliente_id" id="sel1">
            <option value=""> Seleccionar cliente</option>
            <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cliente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($cliente->id); ?>"><?php echo e($loop->iteration); ?>.- <?php echo e($cliente->razon_social); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
        </div>
        <div class="form-group">
          <button class="btn btn-success">Filtrar</button>
        </div>
    </form>
    <div class="animated fadeIn">
      <div class="row bg-white">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Cliente</th>
              <th scope="col">Tipo</th>
              <th scope="col">Nombre</th>
              <th scope="col">Estado</th>
              <th scope="col">Fecha de Entrega</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php $__currentLoopData = $tareas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tarea): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><?php echo e($tarea->cliente->razon_social); ?></td>
                <td><?php echo e($tarea->tipo->tipo); ?> </td>
                <th><?php echo e($tarea->nombre); ?></th>
                <td><?php echo e($tarea->estado->nombre); ?></td>
                <td><?php echo e($tarea->fecha_entrega); ?></td>
                <td>
                  <!-- Button model gestion-->
                  <button type="button" data-toggle="modal" data-target="#gestionarModal<?php echo e($tarea->id); ?>" class="btn btn-primary">Gestionar</button>

                  <!-- Modal Gestion Header-->
                  <div class="modal fade" id="gestionarModal<?php echo e($tarea->id); ?>" tabindex="-1" role="dialog" aria-labelledby="gestionModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                              <h3 class="modal-title" id="gestionModalLabel">Detalles de la Tarea</h3>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            <div class="container-fluid">
                              <div class="row">
                                  <div class="col">
                                    <div class="card-header">
                                      <form name="create_servicio" method="post" action="<?php echo e(route('tarea.actualizar', ['id'=>$tarea->id])); ?>">
                                          <?php echo e(csrf_field()); ?>

                                          <h3 class="card-header">Datos de la Tarea</h3>
                                          <h4 class="card-block">Cliente: <?php echo e($tarea->cliente->razon_social); ?></h4>
                                          <div class="card-block">
                                            <h4>Nombre</h4>
                                            <input type="text" name="nombre" value="<?php echo e($tarea->nombre); ?>" class="form-control" id="inputEmail4" placeholder="nombre">
                                          </div>
                                        <div class="card-block">
                                          <h4>Descripción 
                                            <span class="badge badge-primary"> Versión <?php echo e($tarea->ultimaVersion()->version); ?></span>
                                          </h4>
                                          <textarea class="form-control" id="descripcion" name="descripcion" rows="3"><?php echo e($tarea->ultimaVersion()->descripcion); ?></textarea>
                                          <input type="text" hidden name="descripcion_id" value="<?php echo e($tarea->ultimaVersion()->id); ?>">
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class='col-sm-6'>
                                                  <h4>Fecha de Entrega</h4>
                                                    <input type='date' name="fecha_entrega" value="<?php echo e($tarea->fecha_entrega); ?>" class="form-control" id='datetimepicker4' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <label for="id_categoria">Seleccionar un categoría:</label>
                                            <select class="form-control" name="id_categoria" id="id_categoria">
                                              <option value=""> Seleccionar categoría</option>
                                              <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($categoria->id); ?>" <?php if($categoria->id === $tarea->id_categoria): ?> selected <?php endif; ?>><?php echo e($loop->iteration); ?>.- <?php echo e($categoria->nombre); ?></option>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="card-block">
                                            <label for="id_categoria">Seleccionar un tipo:</label>
                                            <select class="form-control" name="id_tipo" id="id_tipo">
                                              <option value=""> Seleccionar tipo</option>
                                              <?php $__currentLoopData = $tipos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($tipo->id); ?>" <?php if($tipo->id === $tarea->id_tipo): ?> selected <?php endif; ?>><?php echo e($loop->iteration); ?>.- <?php echo e($tipo->tipo); ?></option>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="card-block">
                                            <label for="id_categoria">Seleccionar la fuente:</label>
                                            <select class="form-control" name="id_fuente" id="id_fuente">
                                              <option value=""> Seleccionar fuente</option>
                                              <?php $__currentLoopData = $fuentes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fuente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($fuente->id); ?>" <?php if($fuente->id === $tarea->id_fuente): ?> selected <?php endif; ?>><?php echo e($loop->iteration); ?>.- <?php echo e($fuente->nombre); ?></option>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class=" card-block col-md-6">
                                          <div class="card-block">
                                            <h4>Observaciones</h4>
                                            <textarea class="form-control" id="observacion" name="observacion" rows="3">Ingrese una observación</textarea>
                                          </div>
                                          <div class="card-block">
                                              <label for="id_categoria">Seleccionar un estado:</label>
                                              <select class="form-control" name="id_estado" id="id_estado">
                                                <option value=""> Seleccionar estado</option>
                                                <?php $__currentLoopData = $estados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $estado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($estado->id); ?>" <?php if($estado->id === $tarea->id_estado): ?> selected <?php endif; ?>><?php echo e($loop->iteration); ?>.- <?php echo e($estado->nombre); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                              </select>
                                              <div class="card">
                                              <button type="submit" class="btn btn-primary">Actualizar</button>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-header">
                            <h3 class="modal-title" id="gestionModalLabel">Gestiones de la Tarea</h3>
                          </div>
                          <div class="modal-body">
                            <div class="container-fluid">
                              <div class="row">
                                <div class="col">
                                  <div class="card-header">
                                    <div class="animated fadeIn">
                                      <div class="row bg-white">
                                        <table class="table">
                                          <thead class="thead-dark">
                                            <tr>
                                              <th scope="col">Fecha</th>
                                              <th scope="col">Usuario</th>
                                              <th scope="col">Comentario</th>
                                              <th scope="col">Estado</th>
                                              <th scope="col">Acciones</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php $__currentLoopData = $tarea->gestiones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gestion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <tr>
                                                <td><?php echo e($gestion->created_at); ?></td>
                                                <td><?php echo e($gestion->user->nombre); ?> </td>
                                                <th><?php echo e($gestion->observacion); ?></th>
                                                <td><?php echo e($gestion->gestionable_tipo); ?></td>
                                              </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                    <a href="<?php echo e(route('tarea.index')); ?>" class="btn btn-secondary">Cerrar</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div> 
                        </div>
                      </div>
                    </div>
                  <!-- Modal Gestion Body-->
                  
                  <!-- Button model eliminar -->
                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarModal<?php echo e($tarea->id); ?>">
                    Eliminar
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="eliminarModal<?php echo e($tarea->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Eliminar Tarea <?php echo e($tarea->nombre); ?></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          ¿Está seguro que desea eliminar esta tarea?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                          <a class="btn btn-primary" href="<?php echo e(route('tarea.eliminar', [ 'id'=>$tarea->id])); ?>">
                            Eliminar
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
      </div>
    </div>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>