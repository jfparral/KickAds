<?php $__env->startSection('content'); ?>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.index")); ?>">Clientes</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.crear")); ?>">Agregar Cliente</li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
      </ol>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">
          <h3 class="card-header">Ingresar Nuevo Cliente</h3>
          <form name="new_cliente" method="post" action="<?php echo e(route("cliente.guardar")); ?>" enctype="multipart/form-data" >
                <?php echo e(csrf_field()); ?>

                <div class="card-block">
                  <div class="form-group col-md-6">
                    <h4>Telefono</h4>
                    <input type="text" name="telefono" class="form-control" id="inputEmail4" placeholder="telefono">
                  </div>
                  <div class="form-group col-md-6">
                    <h4>Razón Social</h4>
                    <input type="text" name="razon_social" class="form-control" id="inputEmail4" placeholder="razón social">
                  </div>
                  <div class="form-group col-md-6">
                    <h4>RUC</h4>
                    <input type="number" name="ruc" class="form-control" id="inputEmail4" placeholder="ruc">
                  </div>
                    <div class="form-group col-md-6">
                      <h4>Subir logo</h4>
                      <input type="file" name="logo" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                      <small id="fileHelp" class="form-text text-muted">Elegir una imagen para el logo.</small>
                    </div>
                  <div class="form-group col-md-6">
                    <h4>Dirección</h4>
                    <input type="text" name="direccion" class="form-control" id="inputEmail4" placeholder="dirección">
                  </div>
                  <div class="col-md-6">
                    <a href="<?php echo e(route("cliente.index")); ?>" class="btn btn-secondary">
                        Cancelar
                    </a>
                  <button type="submit" name="save" class="btn btn-primary">Guardar</button>                    
                  </div>
                </div>       
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>