<?php $__env->startSection('content'); ?>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.index")); ?>">Clientes</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route("cliente.editar", ["id" => $cliente->id])); ?>"> Editar</li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
          <div class="btn-group" role="group" aria-label="Button group">
            <a class="btn" href="#"><i class="icon-speech"></i>&nbsp;Comentarios</a>
            <a class="btn" href="#"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Configuraciones</a>
          </div>
        </li>
        </li>
      </ol>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header row">
          <form name="update_cliente" method="post" action="<?php echo e(route("cliente.actualizar", [ "id" => $cliente->id ])); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?>

              <h3 class="card-header">Datos del Cliente(<?php echo e($cliente->id); ?>)</h3>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEmail4">Telefono</label>
                  <input type="text" name="telefono" value="<?php echo e($cliente->telefono); ?>" class="form-control" id="inputEmail4" placeholder="telefono">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail4">Razón Social</label>
                  <input type="text" name="razon_social" value="<?php echo e($cliente->razon_social); ?>" class="form-control" id="inputEmail4" placeholder="razón social">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail4">RUC</label>
                  <input type="number" name="ruc" value="<?php echo e($cliente->ruc); ?>" class="form-control" id="inputEmail4" placeholder="ruc">
                </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Subir logo</label>
                    <input type="file" name="logo" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Elegir una imagen para el logo.</small>
                  </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail4">Dirección</label>
                  <input type="text" value="<?php echo e($cliente->direccion); ?>" name="direccion" class="form-control" id="inputEmail4" placeholder="dirección">
                </div>
              </div>
              <div class="col-md-6">
                    <a href="<?php echo e(route("cliente.index")); ?>" class="btn btn-secondary">
                        Cancelar
                    </a>
                  <button type="submit" name="update" value="<?php echo e($cliente->id); ?>" class="btn btn-primary">Actualizar</button>                    
              </div>
              
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>