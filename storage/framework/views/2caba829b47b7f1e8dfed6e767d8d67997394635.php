<?php $__env->startSection('content'); ?>
      <div class="container-fluid">
            <form name="new_cliente" method="post" action="<?php echo e(route("cliente.guardar")); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEmail4">Telefono</label>
                  <input type="text" name="telefono" class="form-control" id="inputEmail4" placeholder="telefono">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail4">Razón Social</label>
                  <input type="text" name="razon_social" class="form-control" id="inputEmail4" placeholder="razón social">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail4">RUC</label>
                  <input type="number" name="ruc" class="form-control" id="inputEmail4" placeholder="ruc">
                </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Subir logo</label>
                    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Elegir una imagen para el logo.</small>
                  </div>
                <div class="form-group col-md-6">
                  <label for="inputEmail4">Dirección</label>
                  <input type="text" name="direccion" class="form-control" id="inputEmail4" placeholder="dirección">
                </div>
              </div>
              <button type="submit" name="save" class="btn btn-primary">Ingresar</button>
            </form>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>