<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version  v1.0.10
 * @link  http://coreui.io
 * Copyright (c) 2018 creativeLabs Łukasz Holeczek
 * @license  MIT
 -->

<!DOCTYPE html>
<html lang="es">
<!-- Head -->
<?php echo $__env->make('layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php echo $__env->make('layout.app-hearder', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="app-body " >
    <!-- Siderbar -->
    <?php echo $__env->make('layout.siderbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Main content -->
    <main class="main">

      <!-- Tabla de clientes-->
      <?php echo $__env->yieldContent('content'); ?>

    </main>
  </div>
    <?php echo $__env->make('layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <!-- Bootstrap and necessary plugins -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

    <!-- Plugins and scripts required by all views -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

    <!-- CoreUI main scripts -->

    <script src="js/app.js"></script>

    <!-- Plugins and scripts required by this views -->

    <!-- Custom scripts required by this view -->
    <script src="js/views/main.js"></script>
</body>
</html>